#include <MsgBoxConstants.au3>
#include <FileConstants.au3>
#include <GuiTab.au3>


Global $configFilePath = "C:\Users\mgranoa\Desktop\test-autoit-config.ini"
Global $nowDate = ""

; ===== MAIN =====

InitializeAllFromConfig($configFilePath)

HotKeySet("!1", "RunRide")
HotKeySet("!2", "KillDriverServersAndBrowsers")
HotKeySet("!3", "ShowSavedTimesheetFile")
HotKeySet("!4", "DisableZscaler")

While 1
    Sleep($SleepTime)
	
	ProcessBasicTimesheet()	
WEnd

; ==============

; ===== FUNCTIONS =====

Func InitializeAllFromConfig($configFilePath)	
	Global $RunRideCMD = IniRead($configFilePath,"General","RunRIDEcmd","")
	Global $TimesheetDirectory = IniRead($configFilePath,"General","TimesheetDirectory","")
	Global $RunDisableZscalerCmd = IniRead($configFilePath,"General","RunDisableZscalerCmd","")
	Global $SleepTime = IniRead($configFilePath,"General","SleepTime","")
EndFunc

Func RunRide()
	Local $iPID = Run("cmd /k " & $RunRideCMD, "", @SW_SHOWMINIMIZED)
EndFunc

Func KillDriverServersAndBrowsers()
	Local $CMD = "taskkill -IM 'iexplore.exe' -IM 'IEDriverServer.exe' -F"
	Local $PID = Run("cmd /k " & $CMD)
	Sleep($SleepTime)
	ProcessClose($PID)
EndFunc

Func ProcessBasicTimesheet()
	
	If @SEC = 0 AND @MIN = 0 Then
		Local $thisTime = @HOUR & ":" & @MIN
		Local $prevTime = String(Number(@HOUR) - 1) & ":00"
		$prevTime = ($prevTime = "-1:00") ? "23:00" : $prevTime
		
		Local $answer = InputBox("Log your timesheet!", "It is now " & $thisTime & "." & @CRLF & "What did you do last hour? " & $prevTime)			
		SaveInputToFile($answer,$prevTime)
		
		MsgBox($MB_SYSTEMMODAL, "Thank you!", "Your input has been saved!" & @CRLF & "Thank you for logging your timesheet!")
	EndIf		

EndFunc

Func SaveInputToFile($answer,$prevTime)
	$nowDate = @MON & "-" & @MDAY & "-" & @YEAR
	Local $fHandle = FileOpen($TimesheetDirectory & $nowDate & "-Timesheet.txt", $FO_APPEND)
	
	Local $lineToAppend = "[ " & $nowDate & " " & $prevTime & " ] " & $answer
	FileWrite($fHandle,$lineToAppend & @CRLF)
	
	FileClose($fHandle)

EndFunc

Func ShowSavedTimesheetFile()
	
	Local $CMD = "notepad " & $TimesheetDirectory & $nowDate & "-Timesheet.txt"
	
	Run("cmd /c " & $CMD)

EndFunc

Func DisableZscaler()

	ModifyRegistry()	
	ModifyInternetOptions()
	
EndFunc

Func ModifyRegistry()

	Local $iPID = Run("cmd /k " & $RunDisableZscalerCmd)
	Sleep($SleepTime)
	ProcessClose($iPID)
	
EndFunc


Func ModifyInternetOptions()

	;======================
	Sleep($SleepTime)
	Send("{LWIN}")
	Sleep($SleepTime)
	Send("Internet{SPACE}Options")
	Sleep($SleepTime)
	Send("{ENTER}")
	;======================
	Local $hndWnd = WinWait("Internet Properties","")	
	WinActivate($hndWnd,"")
	WinWaitActive($hndWnd,"",10)
	;======================
	Local $hndCtrl = ControlGetHandle($hndWnd,"","[CLASS:SysTabControl32; INSTANCE:1]")	
	_GUICtrlTab_ClickTab($hndCtrl, 4, "left", True)
	ControlClick($hndWnd,"","[TEXT:&LAN settings]")
	;======================
	Local $hndWnd2 = WinWait("Local Area Network (LAN) Settings","",10)
	WinWaitActive($hndWnd2,"",10)
	;======================
	Local $isChecked = ControlCommand($hndWnd2, "", "[CLASS:Button; INSTANCE:3]", "IsChecked", "")
	
	If $isChecked = 1 Then
		ControlClick($hndWnd2,"","[CLASS:Button; INSTANCE:3]")	
	EndIf
	Sleep($SleepTime)
	ControlClick($hndWnd2,"","[CLASS:Button; TEXT:OK]")
	;======================
	WinActivate($hndWnd,"")
	WinWaitActive($hndWnd,"",10)	
	Sleep($SleepTime)
	ControlClick($hndWnd,"","[CLASS:Button; TEXT:OK]")

EndFunc